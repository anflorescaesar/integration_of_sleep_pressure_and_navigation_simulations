from IPython.display import clear_output
import time

class logProcess:
    def __init__(self, max_len):
        self.max_len = max_len
        self.timer = time.time()
        if self.max_len > 1000:
            self.step = int(self.max_len/1000)
        else:
            self.step = 1
            
            
    def start(self):
        self.timer = time.time()
        
        
    def update_progress(self, progress):
        if isinstance(progress, int):
            progress = float(progress)
        if not isinstance(progress, float):
            progress = 0
        if progress < 0:
            progress = 0
        if progress >= 1:
            progress = 1
        return progress
    
    def run(self, ix, textInfo="Progress"):
        if ix % self.step == 0:
            bar_length = 20
            progress = self.update_progress(float(ix / self.max_len))


            delay_time = time.time() - self.timer
            left_progress = 1 - progress
            expected_time = -1
            if progress > 0:
                expected_time = float((left_progress * delay_time)/progress)
            block = int(round(bar_length * progress))
            text = textInfo + ": [{0}] {1:.1f}%".format( "#" * block + "-" * (bar_length - block), progress * 100)
            expTime = "\t expected time {0:2.0f}".format(expected_time)
            print(text, expTime, "sec")
            clear_output(wait = True)