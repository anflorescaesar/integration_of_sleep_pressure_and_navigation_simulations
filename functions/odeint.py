import numpy as np
from functions.logProcess import logProcess



def odeint(function, X0, time, args, log = True, textInfo = "process"):
    X    = []
    dXdt = []
    
    # create solution arrays #
    for i in range(len(X0)):
        X.append(    np.zeros( [len(time)] + list(X0[i].shape) )  )
        dXdt.append( np.zeros( [len(time)] + list(X0[i].shape) )  )
    
    # initialize conditions #
    for i in range(len(X0)):
        X[i][0] = X0[i]
    
    if log:
        logPr = logProcess(len(time))
        
        
    for t in range(1, len(time)):
        if log:
            logPr.run(t, textInfo=textInfo)
            
        dXdt_ = function(X0, t, args)
        
        dt = time[t] - time[t-1]
        
        X0 = []
        for i in range(len(dXdt_)):
            dXdt[i][t] = dXdt_[i]
            X[i][t]    = dXdt[i][t] * dt + X[i][t-1]
            X0.append( X[i][t-1] )
    if log:
        logPr.run(len(time), textInfo=textInfo) 
    return X, dXdt
    